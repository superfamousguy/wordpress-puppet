# See http://rspec-puppet.com/
#     http://rspec-puppet.com/matchers/
require 'spec_helper'

describe 'wordpress' do
  context "wordpress class without any parameters" do
    let(:params) {{ }}

    it { is_expected.to compile.with_all_deps }

    it { is_expected.to contain_class('wordpress::install').that_comes_before('wordpress::config') }
    it { is_expected.to contain_class('wordpress::config') }
    it { is_expected.to contain_class('wordpress::service').that_subscribes_to('wordpress::config') }
    it { is_expected.to contain_class('wordpress') }

    it { is_expected.to contain_exec('install_docker') }
    it { is_expected.to contain_exec('extract_wordpress_files') }

    it { is_expected.to contain_file('/opt/scripts').with_ensure('directory') }
    it { is_expected.to contain_file('/opt/scripts/setup_db.sh') }
    it { is_expected.to contain_file('/usr/local/bin/wp') }
    it { is_expected.to contain_file('/usr/share/docker').with_ensure('directory') }
    it { is_expected.to contain_file('/usr/share/docker/install-docker.sh') }
    it { is_expected.to contain_file('/var/www').with_ensure('directory') }
    it { is_expected.to contain_file('/var/www/html').with_ensure('directory') }
    it { is_expected.to contain_file('/var/www/html/wordpress-files.tgz') }
    it { is_expected.to contain_file('/var/www/html/wp-config.php') }

    it { is_expected.to contain_package('httpd').with_ensure('installed') }
    it { is_expected.to contain_package('mysql').with_ensure('installed') }
    it { is_expected.to contain_package('php').with_ensure('installed') }
    it { is_expected.to contain_package('php-mysql').with_ensure('installed') }

    it { is_expected.to contain_service('docker') }
    it { is_expected.to contain_service('httpd') }
  end
end
