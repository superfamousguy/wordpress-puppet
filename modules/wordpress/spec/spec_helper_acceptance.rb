require 'yaml'
require 'beaker-rspec/spec_helper'
require 'beaker-rspec/helpers/serverspec'

RSpec.configure do |c|

  # Project root
  proj_root = File.expand_path(File.join(File.dirname(__FILE__), '..'))

  hosts.each do |host|
    scp_to(host, File.join(ENV['HOME'], '.ssh/id_rsa'), '/root/.ssh/id_rsa')
    shell('ssh-keyscan -H github.com >> /root/.ssh/known_hosts')
  end

  FIXTURES = YAML.load_file(File.join(proj_root, '.fixtures.yml'))
  MODULES = FIXTURES.fetch('fixtures', {}).fetch('symlinks', {})
  FORGE_MODULES = FIXTURES.fetch('fixtures', {}).fetch('forge_modules', {})
  REPOSITORIES = FIXTURES.fetch('fixtures', {}).fetch('repositories', {})

  # Readable test descriptions
  c.formatter = :documentation

  # Configure all nodes in nodeset
  c.before :suite do

    # Install the module (and all of its custom module dependencies)
    if MODULES.empty?
      puppet_module_install(:source => proj_root, :module_name => 'wordpress')
    end
    MODULES.each do |mname, mdir|
      mpath = File.expand_path(mdir.sub('#{source_dir}', proj_root))
      puppet_module_install(:source => mpath, :module_name => mname,
                            :target_module_path => '/etc/puppetlabs/code/modules')
    end

    # Install any 3rd party dependencies (add additional as needed)
    if ! FORGE_MODULES.empty?
      hosts.each do |host|
        FORGE_MODULES.each do |name, forge_name|
          on host, puppet('module', 'install', forge_name), { :acceptable_exit_codes => [0,1] }
        end
      end
    end

    if ! REPOSITORIES.empty?
      hosts.each do |host|
        shell('yum -y install git')
        REPOSITORIES.each do |name, details|
          shell("[ ! -d /etc/puppetlabs/code/modules/#{name} ] && git clone #{details['repo']} /etc/puppetlabs/code/modules/#{name} || true")
          if details['branch']
            shell("cd /etc/puppetlabs/code/modules/#{name} && git checkout #{details['branch']}")
          end
        end
      end
    end

  end
end
