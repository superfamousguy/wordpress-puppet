require 'puppetlabs_spec_helper/module_spec_helper'
require 'rspec-puppet-facts'
require 'rspec-puppet-utils'
include RspecPuppetFacts

RSpec.configure do |c|
  c.default_facts = {
    operatingsystem: 'CentOS',
    operatingsystemrelease: '6.6',
    operatingsystemmajrelease: '6',
    kernel: 'Linux',
    osfamily: 'RedHat',
    puppetversion: `puppet --version`,
  }
end
