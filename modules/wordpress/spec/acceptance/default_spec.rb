# See http://serverspec.org/resource_types.html
require 'spec_helper_acceptance'

describe 'wordpress class' do

  context 'default parameters' do
    it 'should apply idempotently with no errors' do
      pp = <<-EOS
      class { 'wordpress': }
      EOS
      apply_manifest(pp, :catch_failures => true)
      apply_manifest(pp, :catch_failures => true)
    end

    describe package('php') do
      it { is_expected.to be_installed }
    end

    describe package('php-mysql') do
      it { is_expected.to be_installed }
    end

    describe package('httpd') do
      it { is_expected.to be_installed }
    end

    describe service('httpd') do
      it { is_expected.to be_enabled }
      it { is_expected.to be_running }
    end

    describe service('docker') do
      it { is_expected.to be_enabled }
      it { is_expected.to be_running }
    end

  end
end
