# == Class wordpress::service
#
# This class is meant to be called from wordpress for managing
# services.
#
class wordpress::service {

  service { 'httpd':
    ensure     => running,
    enable     => true,
    hasstatus  => true,
    hasrestart => true,
  }

  service { 'docker':
    ensure     => running,
    enable     => true,
    hasstatus  => true,
    hasrestart => true,
  }
}
