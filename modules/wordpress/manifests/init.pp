# Class: wordpress
# ===========================
#
# Full description of class wordpress here.
#
# Parameters
# ----------
#
# * `sample parameter`
#   Explanation of what this parameter affects and what it defaults to.
#   e.g. "Specify one or more upstream ntp servers as an array."
#
class wordpress (
  $root_password = 'vd5T9AIZ#JItb#3h',
  $db_user = 'wordpress',
  $db_password = '3ASj^TSAy4sfJsz8',
  $db_name = 'wordpress',
  $container_name = 'mysql',
  $container_mysql_port = '3306',
  $host_mysql_port = '3306',
  $mysql_version = '5.7',
  $table_prefix = 'wp_',
  $auth_key = '374c04842dceef3b3f1c939c784bbb17',
  $secure_auth_key = 'd565271b96f5abf715d04e38b699cd7a',
  $logged_in_key = '34eb2a66bcad258607144cd280aafb17',
  $nonce_key = 'd4279690fcfef9538dbbc905388fd47b',
  $auth_salt = 'd976ddbcc9ec4f2485d04bd58f1f5645',
  $secure_auth_salt = 'b24d6ed0c96ca9ce66ebd4681647b493',
  $logged_in_salt = 'bfd958c440411efb333b7be8b7089330',
  $nonce_salt = '5cc803aeea022fafa160613bc7b5dc14',
) {
  class { '::wordpress::install': } ->
  class { '::wordpress::config': } ~>
  class { '::wordpress::service': } ->
  Class['::wordpress']
}
