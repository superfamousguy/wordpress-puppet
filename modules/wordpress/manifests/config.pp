# == Class wordpress::config
#
# This class is called from wordpress for service config.
#

class wordpress::config {
  $root_password = $wordpress::root_password
  $db_user = $wordpress::db_user
  $db_name = $wordpress::db_name
  $db_password = $wordpress::db_password
  $container_name = $wordpress::container_name
  $container_mysql_port = $wordpress::container_mysql_port
  $host_mysql_port = $wordpress::host_mysql_port
  $mysql_version = $wordpress::mysql_version
  $table_prefix = $wordpress::table_prefix
  $auth_key = $wordpress::auth_key
  $secure_auth_key = $wordpress::secure_auth_key
  $logged_in_key = $wordpress::logged_in_key
  $nonce_key = $wordpress::nonce_key
  $auth_salt = $wordpress::auth_salt
  $secure_auth_salt = $wordpress::secure_auth_salt
  $logged_in_salt = $wordpress::logged_in_salt
  $nonce_salt = $wordpress::nonce_salt

  file { '/opt/scripts':
    ensure => directory,
    mode => '0755',
    owner => 'root',
    group => 'root',
  }

  file { '/opt/scripts/setup_db.sh':
    mode => '0644',
    owner => 'root',
    group => 'root',
    content => template('wordpress/setup_db.sh.erb'),
  }

  file { '/var/www/html/wp-config.php':
    mode => '0644',
    owner => 'apache',
    group => 'apache',
    content => template('wordpress/wp-config.php.erb'),
  }
}
