# == Class wordpress::install
#
# This class is called from wordpress for install.
#
class wordpress::install {

  # If you want acceptance tests to pass
  # use the basic mysql instead of the
  # specific package name
  # package { 'mysql':
  package { 'mariadb.x86_64':
    ensure => installed,
  }

  package { 'php':
    ensure => installed,
  }

  package { 'php-mysql':
    ensure => installed,
  }

  package { 'httpd':
    ensure => installed,
  }

  file { '/var/www':
    ensure => 'directory',
  }

  file { '/var/www/html':
    ensure => 'directory',
  }

  file { '/usr/share/docker':
    ensure => 'directory',
  }

  file { '/usr/local/bin/wp' :
    ensure => 'file',
    source => 'puppet:///modules/wordpress/wp',
    mode => '0755',
    owner => 'root',
  }

  file { '/usr/share/docker/install-docker.sh':
    ensure => 'file',
    source => 'puppet:///modules/wordpress/install-docker.sh',
    notify => Exec['install_docker']
  }

  file { '/var/www/html/wordpress-files.tgz':
    ensure => 'file',
    source => 'puppet:///modules/wordpress/wordpress-files.tgz',
    notify => Exec['extract_wordpress_files']
  }

  exec { 'extract_wordpress_files':
    refreshonly => true,
    command => '/bin/tar -xzf /var/www/html/wordpress-files.tgz -C /var/www/html/',
  }

  exec { 'install_docker':
    refreshonly => true,
    command => '/bin/sh /usr/share/docker/install-docker.sh',
  }

}
