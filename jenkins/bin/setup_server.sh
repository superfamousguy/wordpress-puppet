#!/bin/bash
set -x
trap read debug

# Setup the Wordpress Server
sudo yum -y update
sudo yum -y install git
sudo yum -y install puppet
ssh -o StrictHostKeyChecking=no -T bitbucket.org
git clone git@bitbucket.org:superfamousguy/wordpress-puppet.git
sudo mv ./wordpress-puppet/modules/* /etc/puppet/modules
sudo puppet apply /etc/puppet/modules/wordpress/tests/init.pp
sudo /bin/sh /opt/scripts/setup_db.sh
sudo setsebool -P  httpd_can_network_connect_db 1
rm -rf wordpress-puppet/
rm -f .ssh/id_rsa*
