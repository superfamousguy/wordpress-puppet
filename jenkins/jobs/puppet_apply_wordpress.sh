#!/bin/bash
set -x
trap read debug

export HOST_IP="`echo -e "import json\nprint [x['PublicIpAddress'] for x in json.loads(open('/var/lib/jenkins/ec2/${CONFIG_FILE}', 'r').read())['Reservations'][0]['Instances']][0]" | python`"
scp -o StrictHostKeyChecking=no ~/.ssh/id_rsa* ec2-user@${HOST_IP}:.ssh/.
scp -o StrictHostKeyChecking=no $WORKSPACE/jenkins/bin/setup_server.sh ec2-user@${HOST_IP}:.
ssh -o StrictHostKeyChecking=no ec2-user@$HOST_IP /bin/sh setup_server.sh
