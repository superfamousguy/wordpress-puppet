#!/bin/bash
set -x
trap read debug

pip install --upgrade --user awscli
if [ "$REGION" == "us-east-1" ]; then
  ~/.local/bin/aws ec2 run-instances --image-id ami-b63769a1 --count $COUNT --instance-type t2.micro --key-name jenkins --security-groups Wordpress --region $REGION > ec2_host_config.yml
elif [ "$REGION" == "us-west-2" ]; then
  ~/.local/bin/aws ec2 run-instances --image-id ami-133f316a --count $COUNT --instance-type t2.micro --key-name jenkins --security-groups Wordpress --region $REGION > ec2_host_config.yml
fi
sleep 60s
export INSTANCE_ID="`echo -e "import json\nprint [x['InstanceId'] for x in json.loads(open('ec2_host_config.yml', 'r').read())['Instances']][0]" | python`"
~/.local/bin/aws ec2 describe-instances --region $REGION --instance-id $INSTANCE_ID > /var/lib/jenkins/ec2/${CONFIG_FILE}
