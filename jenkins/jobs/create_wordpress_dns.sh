#!/bin/bash
set -x
trap read debug

export HOST_IP="`echo -e "import json\nprint [x['PublicIpAddress'] for x in json.loads(open('/var/lib/jenkins/ec2/${CONFIG_FILE}', 'r').read())['Reservations'][0]['Instances']][0]" | python`"
/usr/local/bin/cli53 rrcreate --wait karlkell.com "$SUBDOMAIN A $HOST_IP"
