#!/bin/bash
set -x
trap read debug

export HOST_IP="`echo -e "import yaml\nprint [x['PublicIpAddress'] for x in yaml.load(open('/var/lib/jenkins/ec2/${CONFIG_FILE}', 'r'))['Reservations'][0]['Instances']][0]" | python`"
wget -O ${WORKSPACE}/weather.json 'http://api.openweathermap.org/data/2.5/weather?lat=42.27&lon=-89.09&appid=7b64276b5833caefca6ca00cd05e4b3f'
export CURRENT_WEATHER="`echo -e "import json\nprint json.loads(open('${WORKSPACE}/weather.json', 'r').read())['weather'][0]['description']" | python`"

ssh -o StrictHostKeyChecking=no ec2-user@${HOST_IP} sudo /usr/local/bin/wp core install --path='/var/www/html' --url="'${SUBDOMAIN}.karlkell.com'" --title="'$TITLE'" --admin_user='admin_user' --admin_email='support@karlkell.com' || true
ssh -o StrictHostKeyChecking=no ec2-user@${HOST_IP} sudo /usr/local/bin/wp post create --path='/var/www/html' --post_title="'Weather in Rockford IL'" --post_excerpt="'Current Weather'" --post_content="'The Current Weather is ${CURRENT_WEATHER}'" --post_type='page' --post_status='publish'
