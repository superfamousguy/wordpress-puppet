BUNDLE := $(shell which bundle 2> /dev/null)
BE := $(BUNDLE) exec

#> installs ruby dependencies
bundle: | _has_bundler
	@$(BUNDLE) check &> /dev/null || \
		(echo "=====> installing ruby dependencies"; $(BUNDLE) install)

_has_bundler:
ifeq ($(BUNDLE),)
	$(error bundler is required. Please gem install bundler)
endif

.PHONY: bundle _has_bundler
