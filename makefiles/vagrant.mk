VAGRANT_MACHINE_NAME := default

_has_vagrant:
ifeq ($shell which vagrant 2> /dev/null),)
	$(error Vagrant command not found. Please install vagrant)
endif

.PHONY: _has_vagrant
